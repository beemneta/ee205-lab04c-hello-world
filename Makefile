all: hello1 hello2

CC = g++

CFLAGS = -o

hello1: hello1.cpp
		$(CC) $(CFLAGS) hello1 hello1.cpp

hello2: hello2.cpp
		$(CC) $(CFLAGS) hello2 hello2.cpp

clean: all
		rm -f *.o hello1 hello2 
