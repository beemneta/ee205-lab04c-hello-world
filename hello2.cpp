///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04 - Hello World C++
//
// @author  Beemnet Alemayehu <beemneta@hawaii.edu> 
// @date    10 Feb 2021 
//////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main(){
   
   std:: cout<<"Hellow World!"<<std::endl;

}

